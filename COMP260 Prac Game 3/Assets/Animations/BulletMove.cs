﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>(); 
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rigidbody.velocity = speed * direction;

	}
	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}

}
